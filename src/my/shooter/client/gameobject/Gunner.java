package my.shooter.client.gameobject;

public interface Gunner {
	
	public int getDamage();

	public void setDamage(int damage);

	public int getShotInterval();

	public void setShotInterval(int shotInterval);

	public int getFireVelocity();

	public void setFireVelocity(int fireVelocity);

	public long getLastShot();

	public void setLastShot(long lastShot);
}
