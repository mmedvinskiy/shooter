package my.shooter.client.gameobject;

import my.shooter.client.DeadAlive;

public class GameObject {
	
	protected long birthTime;
	private DeadAlive deadAlive = DeadAlive.NOT_YET_BORN;
	
	private int x;
	private int y;
	
	private int radius;
	
	public GameObject() {
		super();
	}
	
	public GameObject(long birthTime) {
		super();
		this.birthTime = birthTime;
	}
	
	public GameObject(long birthTime, int x, int y) {
		super();
		this.birthTime = birthTime;
		this.x = x;
		this.y = y;
	}

	public long getBirthTime() {
		return birthTime;
	}
	
	public void setBirthTime(long birthTime) {
		this.birthTime = birthTime;
	}

	public DeadAlive getDeadAlive() {
		return deadAlive;
	}

	public void setDeadAlive(DeadAlive deadAlive) {
		this.deadAlive = deadAlive;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
}
