package my.shooter.client.gameobject;


public class HasHealth extends GameObject {

	public static final int HEALTH_MAXIMUM = 100;
	
	private int health = HEALTH_MAXIMUM; //0..100
	private int shield = 1; //0..100
	
	public HasHealth(long birthTime, int x, int y) {
		super(birthTime, x, y);
	}
	
	public HasHealth() {
		
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getShield() {
		return shield;
	}

	public void setShield(int shield) {
		this.shield = shield;
	}

}
