package my.shooter.client.gameobject;

public interface Moveable {

	public int getX();

	public void setX(int x);
	
	public int getY();

	public void setY(int y);
	
	public int getVelocity();

}
