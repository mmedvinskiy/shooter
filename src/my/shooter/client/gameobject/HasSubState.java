package my.shooter.client.gameobject;


public interface HasSubState {
	
	public GameObjectSubState getSubState();
	
	public void setSubState(GameObjectSubState state);

}
