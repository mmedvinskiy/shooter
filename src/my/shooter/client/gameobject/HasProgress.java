package my.shooter.client.gameobject;

public interface HasProgress {
	
	public long getProgress();

	public void setProgress(long progress);

}
