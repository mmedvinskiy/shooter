package my.shooter.client.gameobject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import my.shooter.client.DeadAlive;
import my.shooter.client.bonus.Bonus;
import my.shooter.client.bonus.BonusSubState;
import my.shooter.client.enemies.bomb.Bomb;
import my.shooter.client.enemies.bomb.BombSubState;
import my.shooter.client.enemies.ufo.Ufo;
import my.shooter.client.explosion.Explosion;
import my.shooter.client.fire.Fire;

public class GameObjectPool {

	//private static int count = 0;
	
	private Map<Class<?>, LinkedList<?>> pool = new HashMap<Class<?>, LinkedList<?>>();

	@SuppressWarnings("unchecked")
	public <T extends GameObject> T createObject(Class<T> clazz) {

		T instance;
		@SuppressWarnings("rawtypes")
		LinkedList list = pool.get(clazz);
		if (list == null || list.isEmpty()) {

			instance = createNewObject(clazz);

		} else {
			instance = (T) list.removeFirst();
		}
		
		instance.setDeadAlive(DeadAlive.NOT_YET_BORN);
		return instance;
	}

	@SuppressWarnings("unchecked")
	private <T> T createNewObject(Class<T> clazz) {
		
		if (Fire.class.equals(clazz)) {
			return (T) new Fire();
		}
		if (Explosion.class.equals(clazz)) {
			return (T) new Explosion();
		}
		if (Bonus.class.equals(clazz)) {
			return (T) new Bonus();
		}
		if (Ufo.class.equals(clazz)) {
			return (T) new Ufo();
		}
		if (Bomb.class.equals(clazz)) {
			return (T) new Bomb();
		}
		
		throw new IllegalArgumentException("Object of unknown type" + clazz);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void putObject(Object object) {
		
		LinkedList list = pool.get(object.getClass());
		if (list == null) {
			list = new LinkedList();
			pool.put(object.getClass(), list);
		}

		list.addLast(object);
	}

	public Fire createFire(long birthTime, int x, int y, double angle,
			int damage, int velocity, boolean enemyFire) {
		Fire fire = createObject(Fire.class);

		fire.setBirthTime(birthTime);
		fire.setX(x);
		fire.setY(y);

		fire.setAngle(angle);
		fire.setDamage(damage);
		fire.setVelocity(velocity);
		fire.setEnemyFire(enemyFire);

		return fire;
	}

	public Explosion createExplosion(long birthTime, int x, int y, boolean light) {
		Explosion explosion = createObject(Explosion.class);

		explosion.setProgress(0);
		explosion.setBirthTime(birthTime);
		explosion.setX(x);
		explosion.setY(y);
		explosion.setLight(light);

		return explosion;
	}

	public Ufo createUfo(long birthTime, int x, int y, double angle) {
		Ufo ufo = createObject(Ufo.class);

		ufo.setBirthTime(birthTime);
		ufo.setX(x);
		ufo.setY(y);
		ufo.setAngle(angle);
		ufo.setHealth(HasHealth.HEALTH_MAXIMUM);

		return ufo;
	}

	public Bomb createBomb(long birthTime, int x, int y) {
		Bomb bomb = createObject(Bomb.class);

        bomb.setSubState(BombSubState.WAIT);		
		bomb.setProgress(0);
		bomb.setBirthTime(birthTime);
		bomb.setX(x);
		bomb.setY(y);
		bomb.setHealth(HasHealth.HEALTH_MAXIMUM);

		return bomb;
	}

	public Bonus createBonus(long birthTime, int x, int y) {
		Bonus bonus = createObject(Bonus.class);

		bonus.setSubState(BonusSubState.WAIT);		
		bonus.setProgress(0);
		bonus.setBirthTime(birthTime);
		bonus.setX(x);
		bonus.setY(y);

		return bonus;

	}
}
