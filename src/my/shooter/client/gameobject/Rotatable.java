package my.shooter.client.gameobject;

public interface Rotatable {

	public double getAngle();

	public void setAngle(double angle);
	
	public int getX();

	public int getY();
}