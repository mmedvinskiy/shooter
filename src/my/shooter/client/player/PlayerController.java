package my.shooter.client.player;

import my.shooter.client.AbstractBaseController;
import my.shooter.client.BattleFieldIfc;
import my.shooter.client.Commands;

public class PlayerController extends AbstractBaseController<Player>{

	private static final int BONUS_DURATION = 100;
	
	private final Commands commands;
	
	public PlayerController(BattleFieldIfc battleField, Commands commands) {
		super(battleField);
		this.commands = commands;
	}

	@Override
	protected boolean nextAlive(Player player, long time) {
		
		int up = commands.getUp();
		int right = commands.getRight();
		
		int sqr = up * up + right * right;
		double q = sqr == 0 ? 0 : 1.0 / Math.sqrt(sqr);
		
		int y = (int) (player.getY() - player.getVelocity() * up * q);
		y = y < battleField.getHeight() - player.getRadius() ? y : battleField.getHeight() - player.getRadius();
		y = y > player.getRadius() ? y : player.getRadius();
		player.setY(y);
		
		int x = (int) (player.getX() + player.getVelocity() * right * q);
		x = x < battleField.getWidth() - player.getRadius() ? x : battleField.getWidth() - player.getRadius();
		x = x > player.getRadius() ? x : player.getRadius();
		player.setX(x);
		
		if (commands.isShot()) {
			player.setWaitingForShot(true);
		}
		
		if (player.isHasBonus()) {
			if (time - player.getTimeGotBonus() > BONUS_DURATION) {
				player.setHasBonus(false);
			}
		}
		
		if (player.isWaitingForShot() && (time - player.getLastShot() > player.getShotInterval())) {
			player.setWaitingForShot(false);
			player.setLastShot(time);
			
			shoot(player, time);
		}
		
		return false;
	}

	private void shoot(Player player, long time) {
		if (player.isHasBonus()) {
			battleField.fire(player.getX(), player.getY(), Math.PI, player.getDamage(), player.getFireVelocity(), false);
			battleField.fire(player.getX(), player.getY(), Math.PI * 0.75, player.getDamage(), player.getFireVelocity(), false);
			battleField.fire(player.getX(), player.getY(), Math.PI * 1.25, player.getDamage(), player.getFireVelocity(), false);
		}else{
  		    battleField.fire(player.getX(), player.getY(), Math.PI, player.getDamage(), player.getFireVelocity(), false);
		}
	}

}
