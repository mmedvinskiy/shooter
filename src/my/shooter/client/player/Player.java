package my.shooter.client.player;

import my.shooter.client.gameobject.Gunner;
import my.shooter.client.gameobject.HasHealth;
import my.shooter.client.gameobject.Moveable;

public class Player extends HasHealth implements Moveable, Gunner{

	//Properties
	private int damage;
	private int velocity;
	private int shotInterval;
	private int fireVelocity;
	
	//State
	private long lastShot = -1;
	private boolean waitingForShot = false;

	private boolean hasBonus = false;
	private int timeGotBonus;
	
	public Player(int x, int y) {
		super(0, x, y);
	}

	public Player() {
		super();
	}
	
	@Override
	public long getLastShot() {
		return lastShot;
	}

	@Override
	public void setLastShot(long lastShot) {
		this.lastShot = lastShot;
	}

	public boolean isWaitingForShot() {
		return waitingForShot;
	}

	public void setWaitingForShot(boolean waitingForShot) {
		this.waitingForShot = waitingForShot;
	}

	@Override
	public int getDamage() {
		return damage;
	}

	@Override
	public void setDamage(int damage) {
		this.damage = damage;
	}

	@Override
	public int getVelocity() {
		return velocity;
	}

	public void setVelocity(int velocity) {
		this.velocity = velocity;
	}

	@Override
	public int getShotInterval() {
		return shotInterval;
	}

	@Override
	public void setShotInterval(int shotInterval) {
		this.shotInterval = shotInterval;
	}

	@Override
	public int getFireVelocity() {
		return fireVelocity;
	}

	@Override
	public void setFireVelocity(int fireVelocity) {
		this.fireVelocity = fireVelocity;
	}

	public boolean isHasBonus() {
		return hasBonus;
	}

	public void setHasBonus(boolean hasBonus) {
		this.hasBonus = hasBonus;
	}

	public int getTimeGotBonus() {
		return timeGotBonus;
	}

	public void setTimeGotBonus(int timeGotBonus) {
		this.timeGotBonus = timeGotBonus;
	}
	
}
