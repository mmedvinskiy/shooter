package my.shooter.client;

import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.HasProgress;

public class WaitingController<T extends GameObject & HasProgress> extends AbstractBaseController<T> {

	private final long progressMax;
	
	public WaitingController(BattleFieldIfc battleField) {
		super(battleField);
		this.progressMax = -1;
	}
	
	public WaitingController(BattleFieldIfc battleField, int progressMax) {
		super(battleField);
		this.progressMax = progressMax;
	}

	@Override
	protected boolean nextAlive(T go, long time) {
		boolean completed = false;
		long progress = go.getProgress() + 1;
		
		if ((progress > progressMax) && (progressMax != -1)) {
//			progress = 0;
			completed = true;
		}
		
		go.setProgress(progress);
		return completed;
	}

}
