package my.shooter.client;

import my.shooter.client.gameobject.GameObject;
import my.shooter.client.images.ImageCollectionIfc;

public abstract class AbstractImageCollectionBasedDecorator<T extends GameObject> implements DecoratorIfc<T> {

	protected final ImageCollectionIfc imageCollection;
	
	public AbstractImageCollectionBasedDecorator(ImageCollectionIfc imageCollection) {
		super();
		this.imageCollection = imageCollection;
	}

}
