package my.shooter.client;

import java.util.ArrayList;
import java.util.List;

import my.shooter.client.CommandsIfc.Move;
import my.shooter.client.enemies.bomb.Bomb;
import my.shooter.client.enemies.boss.Boss;
import my.shooter.client.enemies.ufo.Ufo;
import my.shooter.client.gameobject.HasHealth;
import my.shooter.client.player.Player;

import org.vaadin.gwtgraphics.client.DrawingArea;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.KeyCodeEvent;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Shooter implements EntryPoint {

	private VerticalPanel mainPanel = new VerticalPanel();
	private DrawingArea canvas;

	/**
	 * This is the entry point method.
	 */
	@Override
	public void onModuleLoad() {
		
		//GameObjectPool.getInstance();
		
		RootPanel canvasPanel = RootPanel.get("canvas");
		canvasPanel.add(mainPanel);
		canvas = new DrawingArea(Window.getClientWidth(), Window.getClientHeight());
		mainPanel.add(canvas);
				
		final Commands commands = new Commands();
		
		RootPanel rootPanel = RootPanel.get();
		
		rootPanel.addDomHandler(new KeyDownHandler() {
			
			@Override
			public void onKeyDown(KeyDownEvent event) {
				Move move = convertToCommand(event);
				if (move != null) {
					commands.commandReceived(move);
				}
				
				if (isSpace(event)) {
					commands.setShooting(true);
				}
			}

		}, KeyDownEvent.getType());
		
		
		rootPanel.addDomHandler(new KeyUpHandler() {
			
			@Override
			public void onKeyUp(KeyUpEvent event) {
				Move command = convertToCommand(event);
				if ((command != null) /*&& (queue.getMove() == command)*/ ) {
					commands.commandCancelled(command);
					//queue.setMove(null);
				}

				if (isSpace(event)) {
					commands.setShooting(false);
				}			
			}
		}, KeyUpEvent.getType());
		
	
		Game game = new Game(commands);
		GameScenario scenario = createScenario();
		
		game.start(scenario, canvas);
		
	}

	private Move convertToCommand(KeyCodeEvent<?> event) {
		Move command = null;
		if (event.isDownArrow()) {
			command = Move.DOWN;
		}else if (event.isUpArrow()) {
			command = Move.UP;
		}else if (event.isLeftArrow()) {
			command = Move.LEFT;
		}else if (event.isRightArrow()) {
			command = Move.RIGHT;
		}
		return command;
	}
	
	private GameScenario createScenario() {
		
		Player player = new Player();
		player.setVelocity(6);
		player.setDamage(60);
		player.setShotInterval(12);
		player.setFireVelocity(15);
		player.setShield(50);
		player.setRadius(20);

		player.setX(350);
		player.setY(360);

		List<HasHealth> enemies = new ArrayList<HasHealth>();

		
		int timeInterval = 20;
		int time = 0;

		for (int i = 1; i < 20; i++) {
			{
				Ufo ufo1 = new Ufo();
				ufo1.setBirthTime(time);
				ufo1.setVelocity(4);
				ufo1.setAngle(0);
				ufo1.setRadius(20);

				ufo1.setX(Random.nextInt(660) + 20);
				ufo1.setY(20);
				enemies.add(ufo1);
			}
			time += timeInterval;
			
			{
				Bomb bomb = new Bomb();
				bomb.setBirthTime(time);
				bomb.setX(Random.nextInt(660) + 20);
				bomb.setY(Random.nextInt(260) + 20);
				bomb.setRadius(20);
				
				bomb.setFireDamage(10);
				bomb.setFireVelocity(10);
				enemies.add(bomb);
			}
			time += timeInterval;
		}
		
		{
			Boss boss = new Boss();
			boss.setBirthTime(time + 160);
			boss.setVelocity(4);
			boss.setAngle(0);
			boss.setRadius(20);
			
		    boss.setShield(10);

			boss.setX(350);
			boss.setY(40);
			boss.setStartPointX(350);
			boss.setStartPointY(40);

			boss.setDamage(60);
			boss.setShotInterval(4);
			boss.setFireVelocity(15);
			
			enemies.add(boss);
		}

		GameScenario scenario = new GameScenario();
		scenario.setPlayer(player);
		
		
		scenario.setEnemies(enemies);
		
		scenario.setFireCountEstimation(20);
		scenario.setExplosionCountEstimation(20);
		
		return scenario;
		
	}

	private boolean isSpace(KeyCodeEvent<?> event) {
		return event.getNativeKeyCode() == ' ';
	}
}
