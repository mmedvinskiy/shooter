package my.shooter.client;

public interface CommandsIfc {
	
	public boolean isShot();

	public int getUp();

	public int getRight();
	
	public enum Move{
		UP, DOWN, RIGHT, LEFT;
	}
	
	

}
