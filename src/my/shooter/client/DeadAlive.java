package my.shooter.client;

public enum DeadAlive {
	
	NOT_YET_BORN,
	
	ALIVE,
	
	DEAD
}
