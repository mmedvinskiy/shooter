package my.shooter.client;

public class LineMovementStrategy implements MovementStrategyIfc {

	@Override
	public int getX(int x, int y, double ang, int velocity) {
		return (int) (x - velocity * Math.sin(ang));
	}

	@Override
	public int getY(int x, int y, double ang, int velocity) {
		return (int) (y + velocity * Math.cos(ang));
	}

	@Override
	public double getAngle(int x, int y, double ang, int velocity) {
		return ang;
	}

}
