package my.shooter.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.vaadin.gwtgraphics.client.DrawingArea;
import org.vaadin.gwtgraphics.client.Image;
import org.vaadin.gwtgraphics.client.Line;
import org.vaadin.gwtgraphics.client.shape.Text;

import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Timer;

import my.shooter.client.bonus.Bonus;
import my.shooter.client.bonus.BonusSubState;
import my.shooter.client.enemies.bomb.Bomb;
import my.shooter.client.enemies.bomb.BombDetonateController;
import my.shooter.client.enemies.bomb.BombSubState;
import my.shooter.client.enemies.boss.Boss;
import my.shooter.client.enemies.boss.BossSubState;
import my.shooter.client.enemies.boss.HorizontalGlidingController;
import my.shooter.client.enemies.boss.MoveToPlayerController;
import my.shooter.client.enemies.boss.MoveToStartPointController;
import my.shooter.client.enemies.boss.PlaceVerticallyController;
import my.shooter.client.enemies.boss.TrippleGunnerController;
import my.shooter.client.enemies.ufo.Ufo;
import my.shooter.client.enemies.ufo.UfoController;
import my.shooter.client.explosion.Explosion;
import my.shooter.client.explosion.ExplosionController;
import my.shooter.client.explosion.ExplosionDecorator;
import my.shooter.client.fire.Fire;
import my.shooter.client.fire.FireController;
import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.GameObjectPool;
import my.shooter.client.gameobject.HasHealth;
import my.shooter.client.images.ImageCollections;
import my.shooter.client.player.Player;
import my.shooter.client.player.PlayerController;

public class Game implements BattleFieldIfc {

	private static final int REFRESH_INTERVAL = 30;
	private static final int BATTLE_FIELD_WIDTH = 700;
	private static final int BATTLE_FIELD_HEIGHT = 400;

	private static final int COLLISION_DISTANCE = 400; // SQR

	private final List<Fire> fires;
	private final List<HasHealth> enemies;
	private final List<Explosion> explosions;
	private final List<Bonus> bonuses;

	private Player player;

	@SuppressWarnings("rawtypes")
	private Map<Class, ControllerIfc> controllers;
	@SuppressWarnings("rawtypes")
	private Map<Class, DecoratorIfc> decorators;

	private Map<GameObject, Image> images;
	
	private Map<GameObject, Line> linesRed;
	private Map<GameObject, Line> linesGreen;
	
	private Text finalText;

	private Image background1 = new Image(0, 0, 0, 0, "images/space.jpg");
	private Image background2 = new Image(0, 0, 0, 0, "images/space.jpg");

	private final GameObjectPool objectPool = new GameObjectPool();
	private int time = 0;
	private double scale;

	public Game(Commands commands) {

		createControllers(commands);

		createDecorators();

		enemies = new ArrayList<HasHealth>();
		fires = new ArrayList<Fire>();
		explosions = new ArrayList<Explosion>();
		bonuses = new ArrayList<Bonus>();

		images = new HashMap<GameObject, Image>();
		linesRed = new HashMap<GameObject, Line>();
		linesGreen = new HashMap<GameObject, Line>();
	}

	@SuppressWarnings("rawtypes")
	private void createDecorators() {

		decorators = new HashMap<Class, DecoratorIfc>();
		decorators.put(Player.class, new StaticDecorator<Player>(ImageCollections.PLAYER_IMAGE_COLLECTION));
		decorators.put(Ufo.class, new RotateableObjectDecorator<Ufo>(ImageCollections.SHIP_IMAGE_COLLECTION));
		decorators.put(Explosion.class, new ExplosionDecorator(ImageCollections.EXPLOSION_IMAGE_COLLECTION,
				ImageCollections.EXPLOSION_LIGHT_IMAGE_COLLECTION));
		decorators
				.put(Fire.class, new RotateableObjectDecorator<Fire>(ImageCollections.FIRE_IMAGE_COLLECTION, Math.PI));
		decorators.put(Bomb.class, new ProgressDecorator<Bomb>(ImageCollections.BOMB_IMAGE_COLLECTION));
		decorators.put(Bonus.class, new ProgressDecorator<Bonus>(ImageCollections.BONUS_IMAGE_COLLECTION));
		decorators.put(Boss.class, new RotateableObjectDecorator<Ufo>(ImageCollections.SHIP2_IMAGE_COLLECTION));
	}

	@SuppressWarnings("rawtypes")
	private void createControllers(Commands commands) {

		controllers = new HashMap<Class, ControllerIfc>();
		controllers.put(Player.class, new PlayerController(this, commands));
		controllers.put(Ufo.class, new UfoController(this, new VerticalMovementStrategy()));
		controllers.put(Explosion.class, new ExplosionController(this, 2));
		controllers.put(Fire.class, new FireController(this));
		controllers.put(Bomb.class, createBombController());
		controllers.put(Bonus.class, createBonusController());
		controllers.put(Boss.class, createBossController());
	}

	private CompositeSequentialController<Boss> createBossController() {
		CompositeSequentialController<Boss> bossController = new CompositeSequentialController<Boss>(this);

		bossController.setController(BossSubState.JUMP_TO_PLAYER,
				new CompositeParallelController<Boss>(this).addController(new MoveToPlayerController<Boss>(this))
						.addController(new TrippleGunnerController<Boss>(this)));

		bossController.setController(BossSubState.BLAST_OFF, new MoveToStartPointController(this));

		bossController.setController(BossSubState.PLACE_VERTICALLY, new PlaceVerticallyController<Boss>(this, 0.1));

		bossController.setController(
				BossSubState.MOVE_TO_UPPER_LEFT_CORNER,
				new CompositeParallelController<Boss>(this).addController(
						new HorizontalGlidingController<Boss>(this, 30)).addController(
						new TrippleGunnerController<Boss>(this)));
		bossController.setController(
				BossSubState.MOVE_TO_UPPER_RIGHT_CORNER,
				new CompositeParallelController<Boss>(this).addController(
						new HorizontalGlidingController<Boss>(this, BATTLE_FIELD_WIDTH - 30)).addController(
						new TrippleGunnerController<Boss>(this)));
		bossController.setController(
				BossSubState.MOVE_TO_START,
				new CompositeParallelController<Boss>(this).addController(
						new HorizontalGlidingController<Boss>(this, BATTLE_FIELD_WIDTH / 2)).addController(
						new TrippleGunnerController<Boss>(this)));

		bossController.setNextState(BossSubState.JUMP_TO_PLAYER, BossSubState.BLAST_OFF);
		bossController.setNextState(BossSubState.BLAST_OFF, BossSubState.PLACE_VERTICALLY);
		bossController.setNextState(BossSubState.PLACE_VERTICALLY, BossSubState.MOVE_TO_UPPER_LEFT_CORNER);
		bossController.setNextState(BossSubState.MOVE_TO_UPPER_LEFT_CORNER, BossSubState.MOVE_TO_UPPER_RIGHT_CORNER);
		bossController.setNextState(BossSubState.MOVE_TO_UPPER_RIGHT_CORNER, BossSubState.MOVE_TO_START);
		bossController.setNextState(BossSubState.MOVE_TO_START, BossSubState.JUMP_TO_PLAYER);
		
		return bossController;
	}

	private CompositeSequentialController<Bomb> createBombController() {
		CompositeSequentialController<Bomb> bombController = new CompositeSequentialController<Bomb>(this);
		
		bombController.setController(BombSubState.WAIT, new WaitingController<Bomb>(this, 50));
		bombController.setController(BombSubState.DETONATE, new BombDetonateController(this));
		
		bombController.setNextState(BombSubState.WAIT, BombSubState.DETONATE);
		
		return bombController;
	}

	private CompositeSequentialController<Bonus> createBonusController() {
		CompositeSequentialController<Bonus> bonusController = new CompositeSequentialController<Bonus>(this);
		
		bonusController.setController(BonusSubState.WAIT, new WaitingController<Bonus>(this, 200));
		bonusController.setController(BonusSubState.DIE, new DieController<Bonus>(this));
		
		bonusController.setNextState(BonusSubState.WAIT, BonusSubState.DIE);
		
		return bonusController;
	}
	
	// This method should be invoked once
	public void start(GameScenario scenario, final DrawingArea canvas) {

		scale = calculateScaleAndCropCavas(canvas);
		cropCanvas(canvas);

		enemies.addAll(scenario.getEnemies());
		player = scenario.getPlayer();

		for (int i = 0; i < scenario.getFireCountEstimation(); i++) {
			objectPool.putObject(new Fire());
		}
		for (int i = 0; i < scenario.getExplosionCountEstimation(); i++) {
			objectPool.putObject(new Explosion());
		}

		Timer refreshTimer = new Timer() {
			@Override
			public void run() {

				refreshGameObjects(canvas);
				time++;

				updateBackground();

				checkCollisionsEnemyAndPlayerFire();
				checkCollisionsBonusAndPlayer();
				checkCollisionsEnemyFireAndPlayer();

				removeDeadObjects(fires, canvas);
				removeDeadObjects(enemies, canvas);
				removeDeadObjects(explosions, canvas);
				removeDeadObjects(bonuses, canvas);

				if (bonuses.size() < 1) {
					Bonus bonus = objectPool.createBonus(time + calculateBonusBirthDelay(), Random.nextInt(BATTLE_FIELD_WIDTH - 40) + 20,
							Random.nextInt(BATTLE_FIELD_HEIGHT - 40) + 20);
					bonuses.add(bonus);
				}

			}

		};

		refreshTimer.scheduleRepeating(REFRESH_INTERVAL);

	}

	private int calculateBonusBirthDelay() {
		double rand01 = (double)Random.nextInt(100) / (double)100;
		double period = 200;
		return (int) - (Math.log(1 - rand01) * period);
	}

	private double calculateScaleAndCropCavas(final DrawingArea canvas) {
		double scaleW = (double)canvas.getWidth() / (double)getWidth();
		double scaleH = (double)canvas.getHeight() / (double)getHeight();
		double scale = scaleW < scaleH ? scaleW : scaleH;
		return scale;
	}

	private void cropCanvas(final DrawingArea canvas) {
		canvas.setWidth((int) (BATTLE_FIELD_WIDTH * scale));
		canvas.setHeight((int) (BATTLE_FIELD_HEIGHT * scale));
	}

	private void checkCollisionsEnemyAndPlayerFire() {
		for (Iterator<HasHealth> enemyIterator = enemies.iterator(); enemyIterator.hasNext();) {
			HasHealth enemy = enemyIterator.next();
			if (enemy.getDeadAlive() == DeadAlive.ALIVE) {
				checkCollisionWithFire(enemy, true);
			}
		}
	}

	private void checkCollisionWithFire(HasHealth go, boolean enemy) {
		for (Iterator<Fire> fireIterator = fires.iterator(); fireIterator.hasNext();) {
			Fire fire = fireIterator.next();

			if ((fire.isEnemyFire() == !enemy) && collision(go, fire)) {
				go.setHealth(go.getHealth() - fire.getDamage() / go.getShield());
				Explosion explosion = objectPool.createExplosion(time, go.getX(), go.getY(), go.getHealth() > 0);

				if (go.getHealth() <= 0) {
					go.setDeadAlive(DeadAlive.DEAD);
				}

				explosions.add(explosion);
				fire.setDeadAlive(DeadAlive.DEAD);
			}
		}
	}

	private void checkCollisionsBonusAndPlayer() {
		for (Iterator<Bonus> bonusIterator = bonuses.iterator(); bonusIterator.hasNext();) {
			Bonus bonus = bonusIterator.next();

			if (collision(bonus, player)) {
				bonus.setDeadAlive(DeadAlive.DEAD);
				player.setHasBonus(true);
				player.setTimeGotBonus(time);
			}
		}
	}

	private void checkCollisionsEnemyFireAndPlayer() {
		checkCollisionWithFire(player, false);
	}

	private static boolean collision(GameObject state1, GameObject state2) {
		return ((state1.getX() - state2.getX()) * (state1.getX() - state2.getX()) + (state1.getY() - state2.getY())
				* (state1.getY() - state2.getY()) < COLLISION_DISTANCE);
	}

	private void refreshGameObjects(DrawingArea canvas) {

	    addBackground(canvas);

		if (enemies.isEmpty() && explosions.isEmpty() && fires.isEmpty()) {

			showFinalText(canvas, true);

		} else if (player.getDeadAlive() == DeadAlive.DEAD) {

			showFinalText(canvas, false);

		} else {

			updateGameObject(player, canvas, time);

			for (Fire fire : fires) {
				updateGameObject(fire, canvas, time);
			}

			for (GameObject enemy : enemies) {
				updateGameObject(enemy, canvas, time);
			}

			for (Explosion explosion : explosions) {
				updateGameObject(explosion, canvas, time);
			}

			for (Bonus bonus : bonuses) {
				updateGameObject(bonus, canvas, time);
			}
		}
	}

	private void addBackground(DrawingArea canvas) {
		
		if (background1.getParent() == null) {
			canvas.add(background1);
			canvas.add(background2);
		}
	}

	private void showFinalText(DrawingArea canvas, boolean won) {
		if (finalText == null) {
			finalText = new Text((int) ((BATTLE_FIELD_WIDTH / 2 - 100) * scale),
					(int) ((BATTLE_FIELD_HEIGHT / 2) * scale), won ? "MISSION COMPLETED" : "MISSION FAILED");
			finalText.setFontSize((int) (20 * scale));
			finalText.setStrokeColor("white");
			canvas.add(finalText);
		}
	}

	@SuppressWarnings("unchecked")
	public void updateGameObject(GameObject go, DrawingArea canvas, long time) {

		Image image = images.get(go);

		@SuppressWarnings("rawtypes")
		ControllerIfc controller = controllers.get(go.getClass());
		if (controller == null) {
			throw new IllegalStateException("No controller found for class" + go.getClass());
		}

		@SuppressWarnings("rawtypes")
		DecoratorIfc decorator = decorators.get(go.getClass());
		if (decorator == null) {
			throw new IllegalStateException("No decorator found for class" + go.getClass());
		}

		controller.evaluateNextStep(go, time);

		if (go.getDeadAlive() == DeadAlive.ALIVE) {

			image = decorator.decorate(go, image);
			scaleImage(image);
			if (image.getParent() == null) {
				canvas.add(image);
			}
			images.put(go, image);
			if (go instanceof HasHealth) {
				drawHealthIndicator((HasHealth) go, canvas);
			}
		} else {
			removeObjectFromCanvas(go, canvas);
		}
	}

	private void removeObjectFromCanvas(GameObject go, DrawingArea canvas) {
		Image image = images.get(go);
		if ((image != null) && (image.getParent() != null)) {
			canvas.remove(image);
		}
		removeHealthIndicator(go, canvas);
	}

	private void removeHealthIndicator(GameObject go, DrawingArea canvas) {
		{
			Line line = linesGreen.get(go);
			if ((line != null) && (line.getParent() != null)) {
				canvas.remove(line);
			}
		}
		{
			Line line = linesRed.get(go);
			if ((line != null) && (line.getParent() != null)) {
				canvas.remove(line);
			}
		}
	}

	private void scaleImage(Image image) {
		
		image.setHeight((int) (image.getHeight() * scale));
		image.setWidth((int) (image.getWidth() * scale));
		image.setX((int) (image.getX() * scale));
		image.setY((int) (image.getY() * scale));
	}

	private void drawHealthIndicator(HasHealth goh, DrawingArea canvas) {
		int radius = goh.getRadius();
		int x1 = goh.getX() - radius;
		int x2 = x1 + (2 * radius * goh.getHealth()) / HasHealth.HEALTH_MAXIMUM;
		int x3 = x1 + 2 * radius;

	    drawHealthLine(goh, canvas, x1, x2, linesGreen, "green");
		drawHealthLine(goh, canvas, x2, x3, linesRed, "red");
	}

	private void drawHealthLine(GameObject go, DrawingArea canvas, int x1, int x2, Map<GameObject, Line> lines, String color) {
		Line line = lines.get(go);
		if (line == null) {
			line = new Line(0, 0, 0, 0);
			lines.put(go, line);
		}

		line.setX1((int)(x1 * scale));
		line.setY1((int)((go.getY() - go.getRadius()) * scale));
		line.setX2((int)(x2 * scale));
		line.setY2((int)((go.getY() - go.getRadius()) * scale));
		
		line.setStrokeColor(color);
		line.setStrokeWidth((int) scale);
		
		if (line.getParent() == null) {
			canvas.add(line);
		}
	}

	private void removeDeadObjects(List<? extends GameObject> objects, DrawingArea canvas) {
		for (Iterator<? extends GameObject> iterator = objects.iterator(); iterator.hasNext();) {
			GameObject object = iterator.next();
			if (object.getDeadAlive() == DeadAlive.DEAD) {
				iterator.remove();
				removeObjectFromCanvas(object, canvas);
				objectPool.putObject(object);
			}
		}
	}

	@Override
	public void explosion(int x, int y, boolean light) {
		explosions.add(objectPool.createExplosion(time, x, y, light));
	}

	@Override
	public int getWidth() {
		return BATTLE_FIELD_WIDTH;
	}

	@Override
	public int getHeight() {
		return BATTLE_FIELD_HEIGHT;
	}

	@Override
	public void fire(int x, int y, double angle, int damage, int velocity, boolean enemyFire) {
		fires.add(objectPool.createFire(time, x, y, angle, damage, velocity, enemyFire));
	}

	@Override
	public int getPlayerX() {
		return player.getX();
	}

	@Override
	public int getPlayerY() {
		return player.getY();
	}

	private void updateBackground() {
		
		background1.setWidth((int) (BATTLE_FIELD_WIDTH * scale));
		background1.setHeight((int) (BATTLE_FIELD_HEIGHT * scale));
		
		background2.setWidth((int) (BATTLE_FIELD_WIDTH * scale));
		background2.setHeight((int) (BATTLE_FIELD_HEIGHT * scale));
		
		background1.setY((int) ((time % BATTLE_FIELD_HEIGHT) * scale));
		background2.setY((int) ((time % BATTLE_FIELD_HEIGHT - BATTLE_FIELD_HEIGHT) * scale));
	}

}
