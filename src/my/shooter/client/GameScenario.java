package my.shooter.client;

import java.util.List;

import my.shooter.client.gameobject.HasHealth;
import my.shooter.client.player.Player;

public class GameScenario {

	private Player player;
	
	private List<HasHealth> enemies;
	
	//private int width;
	
	//private int height;
	
	private int fireCountEstimation;
	private int explosionCountEstimation;

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public List<HasHealth> getEnemies() {
		return enemies;
	}

	public void setEnemies(List<HasHealth> enemies) {
		this.enemies = enemies;
	}

	/*
	public int getWidth() {
		return width;
	}*/

	/*
	public void setWidth(int width) {
		//this.width = width;
	}*/

	/*public int getHeight() {
		return height;
	}*/

	/*
	public void setHeight(int height) {
		//this.height = height;
	}*/

	public int getFireCountEstimation() {
		return fireCountEstimation;
	}

	public void setFireCountEstimation(int fireCountEstimation) {
		this.fireCountEstimation = fireCountEstimation;
	}

	public int getExplosionCountEstimation() {
		return explosionCountEstimation;
	}

	public void setExplosionCountEstimation(int explosionCountEstimation) {
		this.explosionCountEstimation = explosionCountEstimation;
	}

}
