package my.shooter.client;

import my.shooter.client.gameobject.GameObject;

import org.vaadin.gwtgraphics.client.Image;

public interface DecoratorIfc<T extends GameObject> {
	
	public Image decorate(T go, Image image);

}
