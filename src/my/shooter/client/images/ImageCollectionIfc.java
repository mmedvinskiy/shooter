package my.shooter.client.images;

import org.vaadin.gwtgraphics.client.Image;

public interface ImageCollectionIfc {
	
	public Image getImage(int x, int y, int index, Image image);
	
	public int size();

}
