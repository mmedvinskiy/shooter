package my.shooter.client.images;


import org.vaadin.gwtgraphics.client.Image;

public class ImageCollections {
	
	//private static int count = 0;
	
	public static final StaticImageCollection PLAYER_IMAGE_COLLECTION = new StaticImageCollection("images/player.png", 36, 32);
	public static final Images8BasedCollection BONUS_IMAGE_COLLECTION = new Images8BasedCollection(2, 8, "bonus/powerups-", 32, 32);
	public static final Images8BasedCollection EXPLOSION_IMAGE_COLLECTION = new Images8BasedCollection(4, 8, "explosion1/Explosions1-", 64, 64);
	public static final Images8BasedCollection EXPLOSION_LIGHT_IMAGE_COLLECTION = new Images8BasedCollection(5, 8, "explosion2/Explosions2-", 64, 65);
	public static final Images8BasedCollection BOMB_IMAGE_COLLECTION = new Images8BasedCollection(2, 8, "bomb2/bomb2-", 32, 32);
	public static final Images8BasedCollection FIRE_IMAGE_COLLECTION = new Images8BasedCollection(5, 8, "fire1/fire1-", 32, 32);
	public static final Images8BasedCollection SHIP_IMAGE_COLLECTION = new Images8BasedCollection(5, 8, "ship1/ship1_0-0-", 36, 36);
	public static final Images8BasedCollection SHIP2_IMAGE_COLLECTION = new Images8BasedCollection(5, 8, "ship2/ships2-", 36, 36);
	
	private static class StaticImageCollection implements ImageCollectionIfc{

		private final String filename;
		private final int width;
		private final int height;
		
		public StaticImageCollection(String filename, int width, int height) {
			super();
			this.filename = filename;
			this.width = width;
			this.height = height;
		}

		@Override
		public Image getImage(int x, int y, int index, Image image) {
			
			if (index != 0) {
				throw new IllegalArgumentException("index schould be only 0");
			}
			
			if (image == null) {
				
				image = new Image(0, 0, width, height, filename);
			}else{
				image.setHeight(height);
				image.setWidth(width);
				image.setHref(filename);
			}
			
			
		    image.setX((int) (x - image.getWidth() * 0.5));
		    image.setY((int) (y - image.getHeight() * 0.5));
		    
		    return image;
		}

		@Override
		public int size() {
			return 1;
		}
		
	}
	
	private static class Images8BasedCollection implements ImageCollectionIfc{

		
		private final int width;
		private final int height;
		private String prefix;
		private final int size;
		
		public Images8BasedCollection(int x, int y, String prefix, int width, int height) {
						
			this.height = height;
			this.width = width;
			this.prefix = prefix;
			this.size = x * y;
		}
		

		/**
		 * x, y - coordinates of center
		 */
		@Override
		public Image getImage(int x, int y, int index, Image image) {
			
		    int i = index / 8; 
		    int j = index % 8; 
	        String href = "images/" + prefix + i + "-" + j + ".png";
			
		    if (image == null) {
				image = new Image(0, 0, width, height, href);
		    }else{
		    	image.setHref(href);
		    	image.setWidth(width);
		    	image.setHeight(height);
		    }
			
		    image.setX((int) (x - image.getWidth() * 0.5));
		    image.setY((int) (y - image.getHeight() * 0.5));
			
			return image;
		}


		@Override
		public int size() {
			return size;
		}
		
	}

}
