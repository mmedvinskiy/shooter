package my.shooter.client.enemies.bomb;

import my.shooter.client.AbstractBaseController;
import my.shooter.client.BattleFieldIfc;
import my.shooter.client.DeadAlive;

public class BombDetonateController extends AbstractBaseController<Bomb>{

	public BombDetonateController(BattleFieldIfc battleField) {
		super(battleField);
	}

	@Override
	protected boolean nextAlive(Bomb bomb, long time) {
		
		if (bomb.getSubState() != BombSubState.DETONATE) {
			throw new IllegalArgumentException("This controller is only for Bomb in state DETONATE");
		}
		
		bomb.setDeadAlive(DeadAlive.DEAD);

		battleField.fire(bomb.getX(), bomb.getY(), 0, bomb.getFireDamage(), bomb.getFireVelocity(), true);
		battleField.fire(bomb.getX(), bomb.getY(), Math.PI * 0.25, bomb.getFireDamage(), bomb.getFireVelocity(), true);
		battleField.fire(bomb.getX(), bomb.getY(), Math.PI * 0.5, bomb.getFireDamage(), bomb.getFireVelocity(), true);
		battleField.fire(bomb.getX(), bomb.getY(), Math.PI * 0.75, bomb.getFireDamage(), bomb.getFireVelocity(), true);
		battleField.fire(bomb.getX(), bomb.getY(), Math.PI, bomb.getFireDamage(), bomb.getFireVelocity(), true);
		battleField.fire(bomb.getX(), bomb.getY(), Math.PI * 1.25, bomb.getFireDamage(), bomb.getFireVelocity(), true);
		battleField.fire(bomb.getX(), bomb.getY(), Math.PI * 1.5, bomb.getFireDamage(), bomb.getFireVelocity(), true);
		battleField.fire(bomb.getX(), bomb.getY(), Math.PI * 1.75, bomb.getFireDamage(), bomb.getFireVelocity(), true);
		
		battleField.explosion(bomb.getX(), bomb.getY(), false);
		
		boolean completed = true;
		return completed;
	}

}
