package my.shooter.client.enemies.bomb;

import my.shooter.client.gameobject.GameObjectSubState;
import my.shooter.client.gameobject.HasHealth;
import my.shooter.client.gameobject.HasProgress;
import my.shooter.client.gameobject.HasSubState;

public class Bomb extends HasHealth implements HasProgress, HasSubState {
	
	//properties
	private int fireVelocity;
	private int fireDamage;
	
	//state
    private long progress = 0;
    private BombSubState subState = BombSubState.WAIT;
    
    public Bomb() {
		super();
	}

	@Override
	public long getProgress() {
		return progress;
	}

	@Override
	public void setProgress(long progress) {
		this.progress = progress;		
	}

	@Override
	public BombSubState getSubState() {
		return subState;
	}

	@Override
	public void setSubState(GameObjectSubState state) {
		subState = (BombSubState) state;
	}

	public int getFireVelocity() {
		return fireVelocity;
	}

	public void setFireVelocity(int fireVelocity) {
		this.fireVelocity = fireVelocity;
	}

	public int getFireDamage() {
		return fireDamage;
	}

	public void setFireDamage(int fireDamage) {
		this.fireDamage = fireDamage;
	}
}
