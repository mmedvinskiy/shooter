package my.shooter.client.enemies.bomb;

import my.shooter.client.gameobject.GameObjectSubState;

public enum BombSubState implements GameObjectSubState{
	
	WAIT,
	
	DETONATE

}
