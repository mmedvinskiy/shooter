package my.shooter.client.enemies.ufo;

import my.shooter.client.gameobject.HasHealth;
import my.shooter.client.gameobject.Rotatable;

public class Ufo extends HasHealth implements Rotatable {

	//Properties
	private int velocity;
	
	//State
	private double angle;
	
	@Override
	public double getAngle() {
		return angle;
	}

	@Override
	public void setAngle(double angle) {
		this.angle = angle;
	}

	public int getVelocity() {
		return velocity;
	}

	public void setVelocity(int velocity) {
		this.velocity = velocity;
	}
	
}
