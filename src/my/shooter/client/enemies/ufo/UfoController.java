package my.shooter.client.enemies.ufo;

import my.shooter.client.AbstractBaseController;
import my.shooter.client.BattleFieldIfc;
import my.shooter.client.DeadAlive;
import my.shooter.client.MovementStrategyIfc;

public class UfoController extends AbstractBaseController<Ufo> {

	private final MovementStrategyIfc movementStrategy;
	
	public UfoController(BattleFieldIfc battleField, MovementStrategyIfc movementStrategy) {
        super(battleField);
		this.movementStrategy = movementStrategy;
	}

	@Override
	protected boolean nextAlive(Ufo ufo, long time) {
		int x = movementStrategy.getX(ufo.getX(), ufo.getY(), ufo.getAngle(), ufo.getVelocity());
		int y = movementStrategy.getY(ufo.getX(), ufo.getY(), ufo.getAngle(), ufo.getVelocity());
		double angle = movementStrategy.getAngle(ufo.getX(), ufo.getY(), ufo.getAngle(), ufo.getVelocity());

		if ((x > battleField.getWidth()) || (y > battleField.getHeight()) || (x < 0) || (y < 0)) {
			ufo.setDeadAlive(DeadAlive.DEAD);
		}
		
		ufo.setX(x);
		ufo.setY(y);
		
		ufo.setAngle(angle);
		
		return false;
	}


}
