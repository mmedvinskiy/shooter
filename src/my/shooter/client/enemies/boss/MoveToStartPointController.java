package my.shooter.client.enemies.boss;

import my.shooter.client.AbstractBaseController;
import my.shooter.client.BattleFieldIfc;

public class MoveToStartPointController extends AbstractBaseController<Boss> {

	private static final int DISTNACE_START_POINT_REACHED = 4;
	private static final int DISTANCE_TO_SLOW_DOWN = 10;

	public MoveToStartPointController(BattleFieldIfc battleField) {
		super(battleField);
	}

	@Override
	protected boolean nextAlive(Boss go, long time) {
		
		int y = go.getY();
		int x = go.getX();
		
		int destX = go.getStartPointX();
		int destY = go.getStartPointY();
		
		double velocity = 2 * go.getVelocity();
		
		double d = Math.sqrt((x - destX) * (x - destX) + (y - destY) * (y - destY));
		
		if (d < DISTNACE_START_POINT_REACHED) {
			return true;
		}
		if (d < DISTANCE_TO_SLOW_DOWN) {
			velocity = velocity / 2;
		}
		
		int deltaX = destX - x;
		int deltaY = destY - y;
		double angle = Utils.calculateAngle(deltaX, deltaY);
		
		double currentAngle = go.getAngle();
		angle = Utils.calculateWeightedAverage(angle, currentAngle);
		go.setAngle(angle);
		
		go.setX((int) (x - velocity * Math.sin(angle)));
		go.setY((int) (y + velocity * Math.cos(angle)));
		
		return false;
	}

}
