package my.shooter.client.enemies.boss;

import my.shooter.client.AbstractBaseController;
import my.shooter.client.BattleFieldIfc;
import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.Moveable;
import my.shooter.client.gameobject.Rotatable;

public class MoveToPlayerController<T extends GameObject & Moveable & Rotatable> extends AbstractBaseController<T> {

	private static final int CRITICAL_DISTANCE = 10;

	public MoveToPlayerController(BattleFieldIfc battleField) {
		super(battleField);
	}

	@Override
	protected boolean nextAlive(T go, long time) {
		
		int y = go.getY();
		int x = go.getX();
		
		if (y > battleField.getHeight() - go.getRadius()) {
			return true;
		}
		
		int playerX = battleField.getPlayerX();
		int playerY = battleField.getPlayerY();
		
		double d = Math.sqrt((x - playerX) * (x - playerX) + (y - playerY) * (y - playerY));
		
		if (d < CRITICAL_DISTANCE) {
			return true;
		}
		
		int deltaX = playerX - x;
		int deltaY = playerY - y;
		double angle = Utils.calculateAngle(deltaX, deltaY);
		
		if ((Math.PI * 0.25 < angle) && (angle <= Math.PI)) {
			angle = Math.PI * 0.25; 
		}else if ((Math.PI < angle) && (angle < Math.PI * 1.75)){
			angle = Math.PI * 1.75; 
		}else if ((- Math.PI <= angle) && (angle < - Math.PI * 0.25)){
			angle = Math.PI * 1.75; 
		}
		
		double currentAngle = go.getAngle();
		angle = Utils.calculateWeightedAverage(angle, currentAngle);
		go.setAngle(angle);
		
		go.setX((int) (x - go.getVelocity() * Math.sin(angle)));
		go.setY((int) (y + go.getVelocity() * Math.cos(angle)));
		
		return false;
	}

}
