package my.shooter.client.enemies.boss;

import my.shooter.client.gameobject.GameObjectSubState;

public enum BossSubState implements GameObjectSubState{
    
	AIR_ATTACK,
	
    JUMP_TO_PLAYER,
    
    BLAST_OFF,
    
    PLACE_VERTICALLY,
    
    MOVE_TO_UPPER_LEFT_CORNER,

    MOVE_TO_UPPER_RIGHT_CORNER,
    
    MOVE_TO_START
}
