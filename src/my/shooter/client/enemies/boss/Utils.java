package my.shooter.client.enemies.boss;

public class Utils {

	public static double calculateArithmeticMean(double angle, double currentAngle) {
		 
		double mean = (angle + currentAngle) / 2;
		if (mean < 0) {
			mean += 2 * Math.PI;
		}
		
		if (Math.abs(currentAngle - mean) > Math.PI * 0.5) {
			mean = mean - Math.PI;
		}
		
		if (mean < 0) {
			mean += 2 * Math.PI;
		}
		
		return mean;
	}

	public static double calculateWeightedAverage(double angle, double currentAngle) {
		
		return calculateArithmeticMean(calculateArithmeticMean(angle, currentAngle), currentAngle);
	}

	public static double calculateAngle(int deltaX, int deltaY) {
		double atan = Math.atan((double)deltaX / (double)deltaY);
		
		double angle = atan < 0 ? - atan : 2 * Math.PI - atan;
		if (deltaY < 0) {
			angle -= Math.PI;
		}
		return angle;
	}
}
