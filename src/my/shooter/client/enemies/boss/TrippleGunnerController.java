package my.shooter.client.enemies.boss;

import my.shooter.client.AbstractBaseController;
import my.shooter.client.BattleFieldIfc;
import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.Gunner;
import my.shooter.client.gameobject.Rotatable;

public class TrippleGunnerController<T extends GameObject & Gunner & Rotatable> extends AbstractBaseController<T>{

	public TrippleGunnerController(BattleFieldIfc battleField) {
		super(battleField);
	}

	@Override
	protected boolean nextAlive(T go, long time) {

		if (time - go.getLastShot() > go.getShotInterval()) {
			go.setLastShot(time);
			
			shoot(go, time);
		}
		
		return false;
	}

	private void shoot(T go, long time) {
		battleField.fire(go.getX(), go.getY(), go.getAngle(), go.getDamage(), go.getFireVelocity(), true);
		battleField.fire(go.getX(), go.getY(), go.getAngle() + Math.PI * 0.25, go.getDamage(), go.getFireVelocity(), true);
		battleField.fire(go.getX(), go.getY(), go.getAngle() - Math.PI * 0.25, go.getDamage(), go.getFireVelocity(), true);
	}
}
