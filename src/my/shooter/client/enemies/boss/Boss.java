package my.shooter.client.enemies.boss;

import my.shooter.client.gameobject.GameObjectSubState;
import my.shooter.client.gameobject.Gunner;
import my.shooter.client.gameobject.HasHealth;
import my.shooter.client.gameobject.HasSubState;
import my.shooter.client.gameobject.Moveable;
import my.shooter.client.gameobject.Rotatable;

public class Boss extends HasHealth implements Moveable, Rotatable, HasSubState, Gunner{

	//Properties
	private int damage;
	private int velocity;
	private int shotInterval;
	private int fireVelocity;
	private double angle;
	
	private int startPointX; 
	private int startPointY; 
	
	//State
	private long lastShot = -1;
	private BossSubState subState = BossSubState.MOVE_TO_UPPER_LEFT_CORNER;

	@Override
	public int getVelocity() {
		return velocity;
	}
	
	@Override
	public int getDamage() {
		return damage;
	}

	@Override
	public void setDamage(int damage) {
		this.damage = damage;
	}

	@Override
	public int getShotInterval() {
		return shotInterval;
	}

	@Override
	public void setShotInterval(int shotInterval) {
		this.shotInterval = shotInterval;
	}

	@Override
	public int getFireVelocity() {
		return fireVelocity;
	}

	@Override
	public void setFireVelocity(int fireVelocity) {
		this.fireVelocity = fireVelocity;
	}

	@Override
	public long getLastShot() {
		return lastShot;
	}

	@Override
	public void setLastShot(long lastShot) {
		this.lastShot = lastShot;
	}

	public void setVelocity(int velocity) {
		this.velocity = velocity;
	}

	@Override
	public double getAngle() {
		return angle;
	}

	@Override
	public void setAngle(double angle) {
		this.angle = angle;
	}

	public int getStartPointX() {
		return startPointX;
	}

	public void setStartPointX(int startPointX) {
		this.startPointX = startPointX;
	}

	public int getStartPointY() {
		return startPointY;
	}

	public void setStartPointY(int startPointY) {
		this.startPointY = startPointY;
	}

	@Override
	public GameObjectSubState getSubState() {
		return subState;
	}

	@Override
	public void setSubState(GameObjectSubState state) {
		this.subState = (BossSubState) state;
		
	}

}
