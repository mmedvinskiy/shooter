package my.shooter.client.enemies.boss;

import my.shooter.client.AbstractBaseController;
import my.shooter.client.BattleFieldIfc;
import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.Rotatable;

public class PlaceVerticallyController<T extends GameObject & Rotatable>
		extends AbstractBaseController<T> {

	private final double velocity;

	public PlaceVerticallyController(BattleFieldIfc battleField, double velocity) {
		super(battleField);
		this.velocity = velocity;
	}

	@Override
	protected boolean nextAlive(T go, long time) {

		double angle = go.getAngle();

		if (angle == 0) {
			return true;
		}

		if ((angle > 0) && (angle <= Math.PI)) {
			angle = angle < velocity ? 0 : angle - velocity;
		}

		if ((angle > Math.PI) && (angle < Math.PI * 2)) {
			angle = angle > 2 * Math.PI - velocity ? 0 : angle + velocity;
		}

		go.setAngle(angle);

		return false;
	}

}
