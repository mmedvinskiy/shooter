package my.shooter.client.enemies.boss;

import my.shooter.client.AbstractBaseController;
import my.shooter.client.BattleFieldIfc;
import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.Moveable;

public class HorizontalGlidingController<T extends GameObject & Moveable> extends AbstractBaseController<T> {
	
	public HorizontalGlidingController(BattleFieldIfc battleField, int destX) {
		super(battleField);
		this.destX = destX;
	}

	private final int destX;

	@Override
	protected boolean nextAlive(T go, long time) {
		if (go.getX() == destX) {
			return true;
		}
		
		if (go.getX() > destX) {
			go.setX(go.getX() < destX + go.getVelocity() ? destX : go.getX() - go.getVelocity());
		}else{
			go.setX(go.getX() > destX - go.getVelocity() ? destX : go.getX() + go.getVelocity());
		}
		
		return false;
	}
	
	

}
