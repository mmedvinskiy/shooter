package my.shooter.client;

public class VerticalMovementStrategy implements MovementStrategyIfc {

	@Override
	public int getX(int x, int y, double ang, int velocity) {
		return x;
	}

	@Override
	public int getY(int x, int y, double ang, int velocity) {
		return y + velocity;
	}

	@Override
	public double getAngle(int x, int y, double ang, int velocity) {
		return ang;
	}

}
