package my.shooter.client;

public interface BattleFieldIfc {
	
	public void fire(int x, int y, double angle,
			int damage, int velocity, boolean enemyFire);
	
	public void explosion(int x, int y, boolean light);
	
	public int getWidth();

	public int getHeight();
	
	public int getPlayerX();

	public int getPlayerY();

}
