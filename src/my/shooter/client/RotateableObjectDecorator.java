package my.shooter.client;

import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.Rotatable;
import my.shooter.client.images.ImageCollectionIfc;

import org.vaadin.gwtgraphics.client.Image;

public class RotateableObjectDecorator<T extends GameObject & Rotatable> extends
		AbstractImageCollectionBasedDecorator<T> {
	
	private double shift = 0;

	public RotateableObjectDecorator(ImageCollectionIfc imageCollection) {
		super(imageCollection);
	}

	public RotateableObjectDecorator(ImageCollectionIfc imageCollection, double shift) {
		super(imageCollection);
		this.shift = shift;
	}
	
	@Override
	public Image decorate(T go, Image image) {
	    int size = imageCollection.size();
	    
		int index = (int) (((go.getAngle() + shift) / (2 * Math.PI)) * size); 
		if (index >= size) {
	    	index -= size;
	    }
		if (index < 0) {
	    	index += size;
	    }

		return imageCollection.getImage(go.getX(), go.getY(), index, image);
	}

}
