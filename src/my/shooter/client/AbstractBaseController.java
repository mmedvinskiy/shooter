package my.shooter.client;

import my.shooter.client.gameobject.GameObject;

public abstract class AbstractBaseController<T extends GameObject> implements ControllerIfc<T> {

	protected final BattleFieldIfc battleField;
	
	public AbstractBaseController(BattleFieldIfc battleField) {
		super();
		this.battleField = battleField;
	}

	@Override
	public boolean evaluateNextStep(T go, long time) {

		long birthTime = go.getBirthTime();
		DeadAlive deadAliveState = go.getDeadAlive();

		if (time < birthTime) {
			go.setDeadAlive(DeadAlive.NOT_YET_BORN);
			return false;
		}

		if (deadAliveState == DeadAlive.NOT_YET_BORN) {
			
			go.setDeadAlive(DeadAlive.ALIVE);
			return false;
			
		} 
		
		if (deadAliveState == DeadAlive.ALIVE) {
			return nextAlive(go, time);
		} 

		return false;

	}

	protected abstract boolean nextAlive(T go, long time);

}
