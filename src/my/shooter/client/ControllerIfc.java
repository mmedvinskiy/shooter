package my.shooter.client;

import my.shooter.client.gameobject.GameObject;

public interface ControllerIfc<T extends GameObject> {
	
	public boolean evaluateNextStep(T go, long time);
	
}
