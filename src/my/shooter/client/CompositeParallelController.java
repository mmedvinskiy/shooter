package my.shooter.client;

import java.util.ArrayList;
import java.util.List;

import my.shooter.client.gameobject.GameObject;

public class CompositeParallelController<T extends GameObject> extends AbstractBaseController<T> {

	private final List<ControllerIfc<T>> controllers;
	
	public CompositeParallelController(BattleFieldIfc battleField) {
		super(battleField);

		controllers = new ArrayList<ControllerIfc<T>>();
	}
	
	public CompositeParallelController<T> addController(ControllerIfc<T> controller) {
		controllers.add(controller);
		return this;
	}

	@Override
	protected boolean nextAlive(T go, long time) {
		boolean completed = false;
		for (ControllerIfc<T> controller : controllers) {
			if (controller.evaluateNextStep(go, time)) {
				completed = true;
			}
		}
		return completed;
	}

}
