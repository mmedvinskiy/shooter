package my.shooter.client;

import java.util.HashMap;
import java.util.Map;

import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.GameObjectSubState;
import my.shooter.client.gameobject.HasSubState;

public class CompositeSequentialController<T extends GameObject & HasSubState> extends AbstractBaseController<T> {

	private final Map<GameObjectSubState, ControllerIfc<T>> controllers;
	private final Map<GameObjectSubState, GameObjectSubState> nextStates;
	
	public CompositeSequentialController(BattleFieldIfc battleField) {
		super(battleField);
		controllers = new HashMap<GameObjectSubState, ControllerIfc<T>>();
		nextStates = new HashMap<GameObjectSubState, GameObjectSubState>(); 
	}
	
	public void setController(GameObjectSubState state, ControllerIfc<T> controller) {
		controllers.put(state, controller);
	}

	public void setNextState(GameObjectSubState state, GameObjectSubState nextState) {
		nextStates.put(state, nextState);
	}
	
	@Override	
	protected boolean nextAlive(T go, long time) {
		
		GameObjectSubState state = go.getSubState();
		
		ControllerIfc<T> controller = controllers.get(state);
		boolean completed = controller.evaluateNextStep(go, time);
		
		if (completed) {
			GameObjectSubState nextState = nextStates.get(state);
			if (nextState != null) {
				go.setSubState(nextState);
				return false;
			}else{
				go.setDeadAlive(DeadAlive.DEAD);
			}
			
			return true;
		}
		
		return false;
	}

}
