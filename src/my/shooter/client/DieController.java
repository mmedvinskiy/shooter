package my.shooter.client;

import my.shooter.client.gameobject.GameObject;

public class DieController<T extends GameObject> extends AbstractBaseController<T> {

	public DieController(BattleFieldIfc battleField) {
		super(battleField);
	}

	@Override
	protected boolean nextAlive(T go, long time) {
		go.setDeadAlive(DeadAlive.DEAD);
		return true;
	}

}
