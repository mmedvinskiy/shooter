package my.shooter.client;

import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.HasProgress;
import my.shooter.client.images.ImageCollectionIfc;

import org.vaadin.gwtgraphics.client.Image;

public class ProgressDecorator<T extends GameObject & HasProgress> extends AbstractImageCollectionBasedDecorator<T> {

	
	public ProgressDecorator(ImageCollectionIfc imageCollection) {
		super(imageCollection);
	}

	@Override
	public Image decorate(T go, Image image) {
		int index = (int)(go.getProgress() % imageCollection.size());
		return imageCollection.getImage(go.getX(), go.getY(), index, image);
	}

}
