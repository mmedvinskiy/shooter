package my.shooter.client;

import my.shooter.client.gameobject.GameObject;
import my.shooter.client.images.ImageCollectionIfc;

import org.vaadin.gwtgraphics.client.Image;

public class StaticDecorator<T extends GameObject> extends
		AbstractImageCollectionBasedDecorator<T> {

	public StaticDecorator(ImageCollectionIfc imageCollection) {
		super(imageCollection);
	}

	@Override
	public Image decorate(T go, Image image) {
		return imageCollection.getImage(go.getX(), go.getY(), 0, image);
	}

}
