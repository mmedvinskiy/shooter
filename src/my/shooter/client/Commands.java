package my.shooter.client;

public class Commands implements CommandsIfc {

	private int up = 0; 
    private int down = 0; 
    private int left = 0;
    private int right = 0;

	@Override
	public int getUp() {
		return up - down;
	}

	@Override
	public int getRight() {
		return right - left;
	}

	public void commandReceived(Move move) {
		if (move == null) {
			new IllegalArgumentException("move can not be null");
		}

		if (move == Move.DOWN) {
			down = 1;
		}else if (move == Move.UP) {
			up = 1;
		}else if (move == Move.RIGHT) {
			right = 1;
		}else if (move == Move.LEFT) {
			left = 1;
		}
	}

	public void commandCancelled(Move move) {
		if (move == null) {
			new IllegalArgumentException("move can not be null");
		}

		if (move == Move.DOWN) {
			down = 0;
		}else if (move == Move.UP) {
			up = 0;
		}else if (move == Move.RIGHT) {
			right = 0;
		}else if (move == Move.LEFT) {
			left = 0;
		}
	}

	private boolean shooting = false;

	@Override
	public boolean isShot() {
		return shooting;
	}

	public void setShooting(boolean shooting) {
		this.shooting = shooting;
	}

}
