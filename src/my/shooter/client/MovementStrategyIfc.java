package my.shooter.client;

public interface MovementStrategyIfc {

	public int getX(int x, int y, double ang, int velocity);

	public int getY(int x, int y, double ang, int velocity);

	public double getAngle(int x, int y, double ang, int velocity);

}
