package my.shooter.client.bonus;

import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.GameObjectSubState;
import my.shooter.client.gameobject.HasProgress;
import my.shooter.client.gameobject.HasSubState;

public class Bonus extends GameObject implements HasProgress, HasSubState {

	private long progress = 0;
    private BonusSubState subState = BonusSubState.WAIT;
	
	@Override
	public long getProgress() {
		return progress;
	}

	@Override
	public void setProgress(long progress) {
		this.progress = progress;
	}
	
	
	public Bonus() {
		super();
	}

	@Override
	public GameObjectSubState getSubState() {
		return subState;
	}

	@Override
	public void setSubState(GameObjectSubState state) {
		this.subState = (BonusSubState) state;
	}

}
