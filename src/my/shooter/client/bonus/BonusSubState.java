package my.shooter.client.bonus;

import my.shooter.client.gameobject.GameObjectSubState;

public enum BonusSubState implements GameObjectSubState{
	
	WAIT,
	
	DIE

}
