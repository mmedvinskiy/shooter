package my.shooter.client.fire;

import my.shooter.client.gameobject.GameObject;
import my.shooter.client.gameobject.Rotatable;

public class Fire extends GameObject implements Rotatable {

	//Properties
	private int damage;
	private int velocity;
	private boolean enemyFire;

	//State
	private double angle;
	
	@Override
	public double getAngle() {
		return angle;
	}

	public int getDamage() {
		return damage;
	}

	public int getVelocity() {
		return velocity;
	}

	public boolean isEnemyFire() {
		return enemyFire;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public void setVelocity(int velocity) {
		this.velocity = velocity;
	}

	public void setEnemyFire(boolean enemyFire) {
		this.enemyFire = enemyFire;
	}

	@Override
	public void setAngle(double angle) {
		this.angle = angle;
	}
}
