package my.shooter.client.fire;

import my.shooter.client.AbstractBaseController;
import my.shooter.client.BattleFieldIfc;
import my.shooter.client.DeadAlive;
import my.shooter.client.LineMovementStrategy;

public class FireController extends AbstractBaseController<Fire> {

	public FireController(BattleFieldIfc battleField) {
		super(battleField);
	}

	private LineMovementStrategy movementStrategy = new LineMovementStrategy();  
	
	@Override
	protected boolean nextAlive(Fire fire, long time) {

		int x = movementStrategy.getX(fire.getX(), fire.getY(), fire.getAngle(), fire.getVelocity());
		int y = movementStrategy.getY(fire.getX(), fire.getY(), fire.getAngle(), fire.getVelocity());

		
		fire.setX(x);
		fire.setY(y);
		
		if ((x > battleField.getWidth()) || (x < 0) || (y > battleField.getHeight()) || (y < 0)) {
			fire.setDeadAlive(DeadAlive.DEAD);
			return true;
		}
		
		return false;

	}

}
