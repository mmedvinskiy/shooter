package my.shooter.client.explosion;

import my.shooter.client.gameobject.GameObject;

public class Explosion extends GameObject{

    public static int PROGRESS_MAXIMUM = 100;
    
    //properties
    private boolean light = false;
	
	//State
	private int progress = 0; //0..100
	
	public Explosion() {
		super();
	}
	
	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public boolean isLight() {
		return light;
	}

	public void setLight(boolean light) {
		this.light = light;
	}

}
