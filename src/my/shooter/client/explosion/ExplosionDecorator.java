package my.shooter.client.explosion;

import my.shooter.client.AbstractImageCollectionBasedDecorator;
import my.shooter.client.images.ImageCollectionIfc;

import org.vaadin.gwtgraphics.client.Image;

public class ExplosionDecorator extends
		AbstractImageCollectionBasedDecorator<Explosion> {

	protected final ImageCollectionIfc explosionLightIC;
	
	public ExplosionDecorator(ImageCollectionIfc explosionIC, ImageCollectionIfc explosionLightIC) {
		super(explosionIC);
		this.explosionLightIC = explosionLightIC;
	}

	@Override
	public Image decorate(Explosion explosion, Image image) {
		ImageCollectionIfc ic;
		if (!explosion.isLight()) {
			ic = imageCollection;
		} else {
			ic = explosionLightIC;
		}
		int index = explosion.getProgress() * (ic.size() - 1)
				/ Explosion.PROGRESS_MAXIMUM;
		return ic.getImage(explosion.getX(), explosion.getY(), index, image);
	}

}
