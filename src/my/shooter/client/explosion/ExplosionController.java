package my.shooter.client.explosion;

import my.shooter.client.AbstractBaseController;
import my.shooter.client.BattleFieldIfc;
import my.shooter.client.DeadAlive;

public class ExplosionController extends AbstractBaseController<Explosion> {

	private final int velocity;
	
	public ExplosionController(BattleFieldIfc battleField, int velocity) {
		super(battleField);
		this.velocity = velocity;
	}
	
	public ExplosionController(BattleFieldIfc battleField) {
		this(battleField, 1);
	}
	
	@Override
	protected boolean nextAlive(Explosion state, long time) {
		int progress = state.getProgress() + velocity;
		
		if (progress <= Explosion.PROGRESS_MAXIMUM) {
			state.setProgress(progress);
			
			return false;
		}

		state.setDeadAlive(DeadAlive.DEAD);
		return true;
	}

}
