package my.shooter.client;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import junit.framework.Assert;
import my.shooter.client.fire.Fire;
import my.shooter.client.gameobject.GameObjectPool;

@RunWith(BlockJUnit4ClassRunner.class)
public class TestGameObjectPool {

	@Test
	public void testPool() {
		GameObjectPool pool = new GameObjectPool();
		
		Fire fire = new Fire();
		pool.putObject(fire);
		
		Fire fire2 = pool.createFire(0, 0, 0, 0.0, 0, 0, true);
		Assert.assertSame(fire, fire2);
	}
	
}
